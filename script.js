const quoteContainer = document.getElementById('quote-container');
const quoteText = document.getElementById('quote');
const authorText = document.getElementById('author');
const twitterBtn = document.getElementById('twitter');
const newQuoteBtn = document.getElementById('new-quote');
const loader = document.getElementById('loader');
let retryCounter = 0;

function showLoadingSpinner() {
	loader.hidden = false;
	quoteContainer.hidden = true;
}

function removeLoadingSpinner() {
	if (!loader.hidden) {
		quoteContainer.hidden = false;
		loader.hidden = true;
	}
}

async function getQuoteFromAPI() {
	showLoadingSpinner();
	// We need to use a Proxy URL to make our API call in order to avide CORS
	const proxyUrl = 'https://cors-anywhere.herokuapp.com/';
	const apiUrl =
		'http://api.forismatic.com/api/1.0/?method=getQuote&lang=en&format=json';

	try {
		const response = await fetch(proxyUrl + apiUrl);
		const data = await response.json();
		// Check if Author field is blank and replace it with 'Unknown'
		authorText.innerText = data.quoteAuthor ? data.quoteAuthor : 'Unknown';
		// Dynamically reduce font size for long quotes
		if (data.quoteText.length > 120) {
			quoteText.classList.add('long-quote');
		} else {
			quoteText.classList.remove('long-quote');
		}

		quoteText.innerText = data.quoteText;

		removeLoadingSpinner();
	} catch (err) {
		retryCounter++;
		if (retryCounter <= 5) {
			getQuoteFromAPI();
		} else {
			console.log('erroring');
			removeLoadingSpinner();
			authorText.innerText = 'Could not fetch quote';
		}
		console.log('whoops, no quote', err);
	}
}

// Tweet quote
function tweetQuote() {
	const quote = quoteText.innerText;
	const author = authorText.innerText;
	const twitterurl = `https://twitter.com/intent/tweet?text=${quote} - ${author}`;

	window.open(twitterurl, '_blank');
}

// Event listeners
newQuoteBtn.addEventListener('click', getQuoteFromAPI);
twitterBtn.addEventListener('click', tweetQuote);

// On Load
getQuoteFromAPI();
